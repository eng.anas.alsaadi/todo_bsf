/**
 * @format
 */

import React from 'react';
import MButton from './../src/components/global/MButton';
import {render} from '@testing-library/react-native';
import fireEvent from '@testing-library/react-native/build/fireEvent';

describe('Testing Custom Button Component', () => {

    it('Button rendered and pressed correctly ', async () => {
        const handleOnPressButton = jest.fn();
        const testIdButton = 'ButtonTestID';

        const {getByTestId, toJSON} = render(<MButton onPress={handleOnPressButton}/>);


        const foundButton = getByTestId(testIdButton);
        expect(foundButton).toBeTruthy();

        fireEvent.press(foundButton, {});
        expect(handleOnPressButton).toHaveBeenCalledTimes(1);

        expect(toJSON()).toMatchSnapshot();
    });


});

