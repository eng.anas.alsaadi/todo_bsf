/**
 * @format
 */

import React from 'react';
import MText from './../src/components/global/MText';
import { render} from '@testing-library/react-native';

describe('Testing Custom Text Component', () => {

    it('Render and put text value correctly', async () => {
        const testIdName = 'MTextTestID';
        const MTextValueForTest = 'My Custom Component For Text';
        const {getByTestId, toJSON} = render(<MText text={MTextValueForTest}/>);

        const foundText = getByTestId(testIdName);
        expect(foundText).toBeTruthy();

        expect(getByTestId(testIdName).props.children).toBe(MTextValueForTest);

        expect(toJSON()).toMatchSnapshot()
    });
})
