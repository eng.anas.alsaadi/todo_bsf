/**
 * @format
 */

import React from 'react';
import OptionSetting from './../src/components/app/OptionSetting';
import {render, fireEvent} from '@testing-library/react-native';

describe('Testing OptionSetting Component', () => {

    it('Render and set value  TRUE   correctly', () => {
        const handleOnPressItem = jest.fn();

        const testIdRadio = 'RadioButtonTestID';
        const testIdTouchable = 'TouchTestID';

        const {getByTestId, toJSON} = render(<OptionSetting selected={true} onPressItem={handleOnPressItem}/>);


        const foundRadio = getByTestId(testIdRadio);
        expect(foundRadio).toBeTruthy();

        const foundTouchable = getByTestId(testIdTouchable);
        expect(foundTouchable).toBeTruthy();

        expect(foundRadio.props.value).toEqual(true);

        fireEvent.press(foundTouchable, {});
        expect(handleOnPressItem).toHaveBeenCalledTimes(1);

        expect(toJSON()).toMatchSnapshot();

    });

});
