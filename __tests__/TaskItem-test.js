/**
 * @format
 */

import React from 'react';
import render from '@testing-library/react-native/build/render';
import TaskItem from '../src/components/app/TaskItem';

describe('Testing TaskItem Component', () => {

    it('Render correctly', () => {

        const {toJSON} = render(<TaskItem/>);

        expect(toJSON()).toMatchSnapshot();
    });
});
