/**
 * @format
 */

import React from 'react';
import TaskStatus from './../src/components/app/TaskStatus';
import render from '@testing-library/react-native/build/render';
import colors from '../src/constants/colors';
import fireEvent from '@testing-library/react-native/build/fireEvent';

describe('Testing TaskStatus Component', () => {

    it('Renders and Item Pressed correctly', () => {
        const handleOnPressItem = jest.fn();

        const testIdTouchable = 'TaskStatusTouchableTestID';

        const {getByTestId, toJSON} = render(<TaskStatus item={{backgroundColor: colors.Red}}
                                                         onPressItem={handleOnPressItem}/>);

        const foundTouchable = getByTestId(testIdTouchable);
        expect(foundTouchable).toBeTruthy();


        fireEvent.press(foundTouchable, {});
        expect(handleOnPressItem).toHaveBeenCalledTimes(1);

        expect(toJSON()).toMatchSnapshot();
    });
})
