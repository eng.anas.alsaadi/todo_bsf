/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {FC} from 'react';
import {LogBox} from 'react-native';
import {Provider} from 'react-redux';
import store from './store';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import FlashMessage from 'react-native-flash-message';
import {navigationRef} from './navigation/NavigationService';
import AppNavigator from './navigation';

import './localization/i18n';

LogBox.ignoreAllLogs(true);

const App: FC = () => {

    return (
        <Provider store={store}>
            <SafeAreaProvider>
                <NavigationContainer ref={navigationRef}>
                    <AppNavigator/>
                </NavigationContainer>
                <FlashMessage/>
            </SafeAreaProvider>
        </Provider>
    );
};

export default App;
