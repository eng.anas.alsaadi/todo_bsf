import React, {FC} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import MText from '../../../components/global/MText';
import Fontisto from 'react-native-vector-icons/Fontisto';


type Props = {

    name: string,
    selected: boolean,
    onPressItem: () => void

}
const OptionSetting:FC<Props> = ({name, selected, onPressItem}) => {

    return <TouchableWithoutFeedback testID="TouchTestID" onPress={onPressItem}>
        <View style={styles.Container}>
            <Fontisto testID="RadioButtonTestID" value={selected} name={selected ? 'radio-btn-active' : 'radio-btn-passive'}
                      size={wp(4)}/>
            <MText  type={'R_12'} text={name} moreStyle={{paddingStart: wp(1)}}/>
        </View>
    </TouchableWithoutFeedback>;
};

const styles = StyleSheet.create({

    Container: {
        flexDirection: 'row', alignItems: 'center',
    },

});

export default OptionSetting


