import React,{FC} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import colors from '../../../constants/colors';
import MText from '../../../components/global/MText';
import NavigationService from '../../../navigation/NavigationService';
import {
    NAVIGATION_MANAGE_TASK_SCREEN,
} from '../../../navigation/types';
import {TaskType} from "../../../constants/InterfacesData";


const TaskItem: FC<TaskType> = (item) => {

    const StatusBackGroundColor = ['', colors.RedBackground, colors.OrangeBackground, colors.GreenBackground, colors.BlueBackground];
    return <TouchableWithoutFeedback testID={"navigationTouchableID"} onPress={() => {

        NavigationService.navigate(NAVIGATION_MANAGE_TASK_SCREEN, {taskID: item.id});
    }
    }>
        <View style={[styles.itemContainer, {backgroundColor: StatusBackGroundColor[item.status]}]}>
            <MText type={'B_12'} text={item.title} left/>
            <MText type={'L_10'} text={item.date} left/>
            <MText type={'R_11'} text={item.description} left/>

        </View>
    </TouchableWithoutFeedback>;
};

const styles = StyleSheet.create({

    itemContainer: {
        alignItems: 'flex-start',
        backgroundColor: colors.White,
        borderWidth: 0.5,
        borderColor: colors.GrayBorder,
        borderRadius: wp(3),
        paddingVertical: wp(4),
        paddingHorizontal: wp(4),
        marginBottom: wp(4),
    },
});

export default TaskItem;


