import React, {FC, ReactNode} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import colors from '../../../constants/colors';
import AppStyles from '../../../constants/AppStyles';
import MText from '../../../components/global/MText';
import {StatusType} from '../../../constants/InterfacesData';


type Props = {
    item: StatusType,
    onPressItem: (item: StatusType) => StatusType,
    selectedItem: StatusType,
}
const TaskStatus:FC<Props> = ({item, onPressItem, selectedItem}) => {
    return <TouchableWithoutFeedback testID="TaskStatusTouchableTestID" onPress={() => onPressItem(item)}>
        <View style={AppStyles.CenterContent}><View
            style={[styles.iconContainer, {
                backgroundColor: item.backgroundColor,
                borderColor: item.borderColor,

            }]}>{item.icon}</View>
            <MText type={'R_12'} text={item.name} color={colors.BlueFont}/>
            {selectedItem && selectedItem?.id == item.id && <View style={{
                backgroundColor: colors.Blue,
                width: wp(14),
                height: 0.8,
            }}/>}
        </View></TouchableWithoutFeedback>
}

const styles = StyleSheet.create({

    iconContainer: {

        borderRadius: wp(4),
        paddingVertical: wp(4),
        width: wp(16),
        borderWidth: 0.4,
        marginBottom: wp(1.5),
        ...AppStyles.CenterContent
    },

});

export default TaskStatus;


