import React, {useEffect,FC} from 'react';
import {

    StyleSheet,
} from 'react-native';

import colors from './../../../constants/colors';
import typography from './../../../constants/typography';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {useDispatch, useSelector,} from 'react-redux';
import {showMessage} from 'react-native-flash-message';
import {hideAlertMessage} from '../../../store/appListener/actions';

const FlashMessages:FC<any> = () => {

    // const {visible_alert, status_alert, message_alert} = useSelector((state: IRootState) => state.user.loggedIn);


    const {visible_alert, status_alert, message_alert} = useSelector((state) => state.appListener);
    const dispatch = useDispatch();


    useEffect(() => {
        var message_to_alert = '';
        if (!message_alert) {
            message_to_alert = status_alert === 500 ? "server error" : status_alert === 400 ? "connection error" : '';
        } else {
            message_to_alert = message_alert;
        }

        if (visible_alert) {

            dispatch(hideAlertMessage());
            showMessage({
                message: '',
                description: message_to_alert,
                type: status_alert === 200 ? 'success' : 'danger',
                position: 'bottom',
                textStyle: status_alert === 200 ? styles.textStyleSuccess : styles.textStyle,
                titleStyle: styles.titleStyle,
                style: styles.elementStyle,
                floating: true,
                duration: 3000,
                onHide: () => {

                    dispatch(hideAlertMessage());

                }, onPress: () => {

                    dispatch(hideAlertMessage());

                },
            });
        }

    }, [visible_alert]);


    return <React.Fragment/>;
};


const styles = StyleSheet.create({

    textStyle: {
        ...typography.TextRegular12,
        lineHeight: wp(5),
        color: colors.White

    },
    textStyleSuccess: {
        ...typography.TextRegular12,
        lineHeight: wp(5),
        color: colors.White

    },
    titleStyle: {
        height: 0,


        paddingVertical: wp(0),
        marginVertical: wp(0),
    },
    elementStyle: {
        zIndex: 99999

    },
});


export default FlashMessages;
