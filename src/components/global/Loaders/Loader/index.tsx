import React ,{FC} from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import LottieView from 'lottie-react-native';

const Loader: FC<any> = () => {


    return <LottieView

        style={{
            width: wp(19),
        }}
        autoPlay
        loop={true}
        source={require('../../../../assets/files/load')}
    />;
};


export default Loader;
