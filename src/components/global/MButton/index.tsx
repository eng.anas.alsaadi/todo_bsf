import React, {FC} from 'react';

import {
    StyleSheet,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
import colors from './../../../constants/colors';
import MText from '../MText';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Loader from '../Loaders/Loader';


type Props = {
    text: string,
    onPress: () => void,
    loading: boolean,

}
const MButton: FC<Props> = ({text, onPress, loading}) => {

    return (
        <TouchableWithoutFeedback testID={"ButtonTestID"} onPress={onPress} disabled={loading}>
            <View style={[styles.buttonContainer]}>

                {loading ? <Loader/> : <MText

                    type={'B_13'}
                    color={colors.White}
                    text={text}/>}


            </View>
        </TouchableWithoutFeedback>
    );
};


const styles = StyleSheet.create({

    buttonContainer: {
        borderWidth: 1,
        borderRadius: wp(4),
        borderColor: colors.Blue,
        backgroundColor: colors.Blue,
        alignItems: 'center',
        paddingHorizontal: wp(4),
        flexDirection: 'row',
        justifyContent: 'center',
        height: wp(13),
        marginBottom: wp(4),
    }
});

export default MButton;


