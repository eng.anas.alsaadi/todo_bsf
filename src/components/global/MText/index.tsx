import React, {FC} from 'react';

import {
    ViewStyle, Text,
} from 'react-native';
import colors from './../../../constants/colors';
import typography from './../../../constants/typography';
import {bool} from "prop-types";

type MTextType = {
    text: string;
    type: string;
    center?: boolean;
    left?: boolean;
    right?: boolean;
    color?: string;
    moreStyle?: ViewStyle
};
const MText:FC<MTextType> = ({text,type,center,left,right,color,moreStyle}) => {
    const fontArr = [
        {
            type: 'R_9',
            value: typography.TextRegular9,
        }, {
            type: 'R_10',
            value: typography.TextRegular10,
        }, {
            type: 'R_11',
            value: typography.TextRegular11,
        }, {
            type: 'R_12',
            value: typography.TextRegular12,
        }, {
            type: 'R_13',
            value: typography.TextRegular13,
        }, {
            type: 'R_14',
            value: typography.TextRegular14,
        }, {
            type: 'R_15',
            value: typography.TextRegular15,
        }, {
            type: 'R_16',
            value: typography.TextRegular16,
        }, {
            type: 'B_9',
            value: typography.TextBold9,
        }, {
            type: 'B_10',
            value: typography.TextBold10,
        }, {
            type: 'B_11',
            value: typography.TextBold11,
        }, {
            type: 'B_12',
            value: typography.TextBold12,
        }, {
            type: 'B_13',
            value: typography.TextBold13,
        }, {
            type: 'L_8',
            value: typography.TextLight8,
        }, {
            type: 'L_10',
            value: typography.TextLight10,
        }, {
            type: 'L_11',
            value: typography.TextLight11,
        }, {
            type: 'L_12',
            value: typography.TextLight12,
        },
    ];
    const FontType = type ? fontArr.find(e => e.type === type)?.value : fontArr[0].value;
    const TextAlign = right ? 'right' : left ? 'left' : 'center';
    const FontColor = color ? color : colors.BlueDark;
    return <Text  testID="MTextTestID" style={[

        {
            ...FontType,
            textAlign: TextAlign,
            color: FontColor,
            ...moreStyle,
        },
    ]}>{text}</Text>;
};

export default MText


