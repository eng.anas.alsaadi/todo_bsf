import React, {FC, useState} from 'react';

import {
    TextInput,
    StyleSheet,
    View, TouchableWithoutFeedback, ViewStyle,
} from 'react-native';
import colors from './../../../constants/colors';
import typography from './../../../constants/typography';
import MText from '../MText';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import DatePicker from 'react-native-date-picker';

type Props = {

    type?: string,
    name: string,
    placeholder: string,
    value?: any,
    onChangeText: (value: any) => void,
    multiline?: boolean,
    error?: string

}

const MTextInput: FC<Props> = ({type, name, placeholder, value, onChangeText, multiline, error}) => {


    const [open, setOpen] = useState<boolean>(false);

    return <View style={{marginBottom: wp(4)}}>
        <MText type={'B_13'} text={name} left/>


        {type === 'date' ? <TouchableWithoutFeedback onPress={() => setOpen(true)}><View
            style={{...styles.CustomTextInput, height: wp(11)}}><MText left type={'R_12'}
                                                                       text={typeof value === 'object' ? value.getFullYear() + '-' + value.getMonth() + '-' + value.getDay() + ' ' + value.getHours() + ':' + value.getMinutes() : typeof value === 'string' ? value : placeholder}/><DatePicker
            modal
            mode={'datetime'}
            open={open}
            date={typeof value === 'object' ? value : new Date()}
            onConfirm={(date) => {
                setOpen(false);
                onChangeText(date);
            }}
            onCancel={() => {
                setOpen(false);
            }}
        /></View></TouchableWithoutFeedback> : <TextInput
            placeholderTextColor={colors.GrayFont}
            autoCapitalize='none'
            placeholder={placeholder}
            value={value}
            onChangeText={onChangeText}
            multiline={multiline}
            style={styles.CustomTextInput}/>}

        {error && <MText type={'L_11'} text={error} color={colors.Red} left/>}
    </View>;
};


const styles = StyleSheet.create({

    CustomTextInput: {

        borderBottomWidth: 0.7,
        borderBottomColor: colors.BlueBorder,
        paddingBottom: wp(2),
        ...typography.TextRegular12,
    },

});
export default MTextInput;


