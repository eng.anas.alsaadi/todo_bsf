import React, {FC} from 'react';
import {View,} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import colors from '../../../constants/colors';
import TaskStatus from '../../../components/app/TaskStatus';
import {useTranslation} from 'react-i18next';
import {StatusType} from "../../../constants/InterfacesData";

type Props = {
    onPressItem: (item:StatusType) => StatusType,
    selectedItem: StatusType
}
const TaskStatusList: React.FC<Props> = ({onPressItem, selectedItem}) => {
    const {t} = useTranslation();
    const sizeIcon = wp(7);
    const status = [

        {
            id: 1,
            name: t('taskStatus.toDo'),
            icon: <MaterialIcons name={'list-alt'} size={sizeIcon} color={colors.Red}/>,
            backgroundColor: colors.RedBackground,
            borderColor: colors.Red,
        },
        {
            id: 2,
            name: t('taskStatus.progress'),
            icon: <MaterialCommunityIcons name={'progress-check'} size={sizeIcon} color={colors.Orange}/>,
            backgroundColor: colors.OrangeBackground,
            borderColor: colors.Orange,

        },
        {
            id: 3,
            name: t('taskStatus.done'),
            icon: <MaterialIcons name={'playlist-add-check'} size={sizeIcon} color={colors.Green}/>,
            backgroundColor: colors.GreenBackground,
            borderColor: colors.Green,

        },
        {
            id: 4,
            name: t('taskStatus.archived'),
            icon: <AntDesign name={'switcher'} size={sizeIcon} color={colors.Blue}/>,
            backgroundColor: colors.BlueBackground,
            borderColor: colors.Blue,

        },
    ];


    return (<View style={styles.containerStatus}>
            {status.map((e, i) => {

                return <TaskStatus selectedItem={selectedItem} onPressItem={onPressItem} key={`TaskStatus+${e.id}`}
                                   item={e}/>;
            })}
        </View>

    );
};
const styles = {

    containerStatus: {
        paddingHorizontal: wp(4),
        width: wp(92),
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: wp(7),
    },
};


export default React.memo(TaskStatusList);


