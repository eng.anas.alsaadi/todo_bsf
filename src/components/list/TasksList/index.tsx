import React, {useEffect,FC} from 'react';
import {View, FlatList} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import {getTaskListRequest, getTaskListReset} from '../../../store/task/actions';
import {REFRESH} from '../../../store/actionsType';
import {gql} from '@apollo/client';
import TaskItem from '../../app/TaskItem';
import {TaskType} from '../../../constants/InterfacesData';


const TasksList: FC<any> = () => {

    const dispatch = useDispatch();


    const {statusTasks, statusGetTasks, haveNextPageGetTasks, nextPageGetTasks, tasks, isLoadingGetTasks} = useSelector((state) => state.task);

    const getData = (refresh: boolean, reset: boolean) => {

        if (reset) {
            dispatch(getTaskListReset());
        }
        var page = refresh ? 1 : nextPageGetTasks;
        var status = statusTasks ? statusTasks.id : 0;
        dispatch(getTaskListRequest({
            gql: gql`
                query getAllTasks($limit:Int,$page:Int,$status:Int) {
                    tasks(limit:$limit,page:$page,status:$status) {
                        data{
                            id
                            title
                            description
                            status
                            date
                        }
                    }
                }
            `,
            variables: {
                limit: 10,
                page,
                status,
            },

        }));
    };

    useEffect(() => {

        if (statusGetTasks === REFRESH) {
            getData(true, true);

        }

    }, [statusGetTasks]);

    useEffect(() => {
        getData(true, true);
    }, [statusTasks]);

    useEffect(() => {

        getData(true, true);

    }, []);


    const handleRefresh = () => {

        getData(true, true);

    };
    const handleLoadMore = () => {

        if (haveNextPageGetTasks && !isLoadingGetTasks) {
            getData(false, false);
        }

    };

    const renderItem: React.FC<any> = ({item}) => {

        return (
            <TaskItem
                id={item.id}
                title={item.title}
                description={item.description}
                status={item.status}
                date={item.date}
            />
        );
    };


    return isLoadingGetTasks && tasks.length === 0 ? null :


        <FlatList
            horizontal={false}
            numColumns={1}
            data={tasks}
            keyExtractor={item => 'task' + item.id + Math.random()}
            refreshing={false}
            onRefresh={handleRefresh}
            renderItem={renderItem}
            onEndReachedThreshold={0.01}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            disableVirtualization={false}
            nestedScrollEnabled={true}
            onEndReached={handleLoadMore}
            contentContainerStyle={{
                paddingHorizontal: wp(4),
            }}


        />
        ;

};


export default React.memo(TasksList);


