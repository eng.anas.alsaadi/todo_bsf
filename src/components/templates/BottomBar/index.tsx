import React, {FC} from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import colors from '../../../constants/colors';
import AppStyles from '../../../constants/AppStyles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {addTaskReset, setStatusSelected} from '../../../store/task/actions';
import {useDispatch} from 'react-redux';


type Props = {
    state?: any,
    navigation?: any
}

const BottomBar: FC<Props> = ({state, navigation}) => {

    const dispatch = useDispatch();

    const onNavigate = (index: number) => {
        if (index === 0) {
            dispatch(setStatusSelected(null));
        }

        if (index === 1) {
            dispatch(addTaskReset());
        }
        const event = navigation.emit({
            type: 'tabPress',
            target: state.routes[index].key,
            canPreventDefault: true,
        });

        if (!event.defaultPrevented) {
            navigation.navigate(state.routes[index].name);
        }

    };
    return <View style={styles.BottomBarContainer}>
        <TouchableWithoutFeedback onPress={() => onNavigate(0)}>
            <View style={styles.BottomItem}>
                <FontAwesome name={'tasks'} size={wp(6)}
                             color={state.index === 0 ? colors.BlueFont : colors.Gray}/>
            </View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback onPress={() => onNavigate(1)}>
            <View style={styles.BottomItem}>
                <View style={styles.containerAddTask}>
                    <AntDesign name={'pluscircle'} size={wp(8)}
                               color={colors.Blue}/>
                </View>
            </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => onNavigate(2)}>
            <View style={styles.BottomItem}>
                <MaterialIcons name={'settings'} size={wp(7)}
                               color={state.index === 2 ? colors.BlueFont : colors.Gray}/>

            </View>
        </TouchableWithoutFeedback>
    </View>
        ;
};
const styles = {
    BottomItem: {
        ...AppStyles.CenterContent,
        width: wp(33.333),
    },
    BottomBarContainer: {
        backgroundColor: colors.White,
        width: wp(100),
        flexDirection: 'row',
        borderTopWidth: 0.4,
        borderTopColor: colors.GrayBorder,
        paddingTop: wp(4),
        paddingBottom: wp(6),
    },
    containerAddTask: {
        borderWidth: wp(0.7),
        borderColor: colors.Blue,
        borderRadius: wp(8),
        backgroundColor: colors.White,
        padding: wp(1),
    },

};

export default BottomBar;


