import React, {FC,ReactNode} from 'react';
import {View} from 'react-native';
import AppStyles from '../../../constants/AppStyles';

type Props = {
    children: ReactNode
}
const CardContainer: FC<Props> = ({children}) => {

    return <View style={[AppStyles.BodyContainer, {flex: 1}]}>
        {children}
    </View>
        ;
};

export default CardContainer;


