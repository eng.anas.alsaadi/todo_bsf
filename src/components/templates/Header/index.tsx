import React, {FC} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import colors from '../../../constants/colors';
import MText from './../../../components/global/MText';
import Avatar from './../../../assets/images/avatar.svg';
import Feather from 'react-native-vector-icons/Feather';
import NavigationService from '../../../navigation/NavigationService';

type Props = {
    title: string,
    back?: boolean
}

const Header: FC<Props> = ({title, back}) => {

    return <View style={styles.headerContainer}>

        <View style={styles.headerThirdSection}>
            {back && <TouchableOpacity onPress={() => NavigationService.goBack()}><Feather
                name={'arrow-left'}
                size={wp(6.5)}
                color={colors.BlueDark}/></TouchableOpacity>}
        </View>
        <View style={styles.headerSecondSection}>
            <MText type={'R_13'} text={title}/>
        </View>

        <View style={styles.headerFirstSection}>
            <Avatar width={wp(12)} height={wp(12)}/>
        </View>

    </View>
        ;
};
const styles = {

    headerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: wp(3),
    },

    headerFirstSection: {
        width: wp(25),
        alignItems: 'flex-end',
        paddingEnd: wp(4),
    },
    headerSecondSection: {
        width: wp(50),
    },
    headerThirdSection: {
        width: wp(25),
        alignItems: 'flex-start',
        paddingStart: wp(4),
    },
};

export default Header;


