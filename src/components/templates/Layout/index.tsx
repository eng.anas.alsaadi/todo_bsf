import React, {useState, useEffect, FC, ReactNode} from 'react';
import {View, InteractionManager} from 'react-native';
import Loader from '../../global/Loaders/Loader';
import AppStyles from '../../../constants/AppStyles';
import Header from '../Header';
import SafeAreaView from 'react-native-safe-area-view';
import FlashMessage from './../../../components/global/FlashMessages';

type Props = {

    title: string, back?: boolean, children: ReactNode

}
const Layout: FC<Props> = ({title, back, children}) => {

    const [didFinishInitialAnimation, setDidFinishInitialAnimation] = useState<boolean>(false);

    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            setDidFinishInitialAnimation(true);
        });

    }, []);

    useEffect(() => {


    }, []);

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>


            <Header title={title} back={back}/>

            <View style={{
                flex: 1,
                backgroundColor: '#fafafa',
            }}>{!didFinishInitialAnimation ? <View style={{...AppStyles.CenterContent}}><Loader/></View> :
                <React.Fragment>{children}</React.Fragment>}</View>

            <FlashMessage/>
        </SafeAreaView>
    );
};


export default React.memo(Layout);


