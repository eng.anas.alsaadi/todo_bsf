import colors from "./colors";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

export default {

    CenterContent: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    BodyContainer: {
        backgroundColor: colors.White,
        paddingVertical: wp(4),
        paddingHorizontal: wp(4),
        borderTopWidth: 0.4,
        borderBottomWidth: 0.4,
        borderTopColor: colors.GrayBorder,
        borderBottomColor: colors.GrayBorder,
        marginTop: wp(2),
        marginBottom: wp(2),
    }

};
