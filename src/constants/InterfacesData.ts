import {ReactNode} from 'react';

interface StatusType {

    id: number,
    icon: ReactNode,
    name: string,
    backgroundColor: string,
    borderColor: string,

}

interface TaskType {

    id: number,
    title: string,
    description: string,
    date: string,
    status: number,

}

interface RequestType {

    gql: string,
    variables: [],

}
interface ReduxType {

    type: string,
    payload: any,

}

export {StatusType, TaskType,RequestType,ReduxType};
