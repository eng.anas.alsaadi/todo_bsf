export default {

    BlueDark: '#141b2e',
    Blue: '#5969fe',
    BlueFont: '#1d4a5a',
    BlueBackground: '#eff1fe',
    White: '#fff',
    Red: '#e6567b',
    RedBackground: '#fbe4ea',
    Orange: '#f8cc29',
    OrangeBackground: '#fef8e2',
    Green: '#6cf5c3',
    GreenBackground: '#e3fdf4',
    Gray:'#c4c4c4',
    GrayFont:'#bfbfc0',


    GrayBorder: '#dadad991',
    BlueBorder: '#bdbfd2',


};
