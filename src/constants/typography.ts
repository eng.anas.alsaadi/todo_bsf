import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

const fontNameRegular = 'Cairo-Regular';
const fontNameSemiBold = 'Cairo-Bold';
const fontNameSemiExtraBold = 'Cairo-Black';
const fontNameMedium = 'Cairo-Light';
const fontNameSmal = 'Cairo-Light';

import {I18nManager} from 'react-native';

export default {

    TextBold9: {
        fontSize: wp('2.5%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextBold10: {
        fontSize: wp('3%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextBold11: {
        fontSize: wp('3.5%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextBold12: {
        fontSize: wp('4%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextBold13: {
        fontSize: wp('4.5%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextBold14: {
        fontSize: wp('5%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextBold16: {
        fontSize: wp('6%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextBold18: {
        fontSize: wp('7%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextBold22: {
        fontSize: wp('9%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextBold30: {
        fontSize: wp('12%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextExtraBold12: {
        fontSize: wp('4%'),
        fontFamily: fontNameSemiExtraBold,
        textAlign: 'auto',
    },
    TextExtraBold13: {
        fontSize: wp('4.5%'),
        fontFamily: fontNameSemiExtraBold,
        textAlign: 'auto',
    },
    TextExtraBold14: {
        fontSize: wp('5%'),
        fontFamily: fontNameSemiExtraBold,
        textAlign: 'auto',
    },
    TextExtraBold16: {
        fontSize: wp('6%'),
        fontFamily: fontNameSemiExtraBold,
        textAlign: 'auto',
    },
    TextExtraBold18: {
        fontSize: wp('7%'),
        fontFamily: fontNameSemiExtraBold,
        textAlign: 'auto',
    },
    TextExtraBold20: {
        fontSize: wp('8%'),
        fontFamily: fontNameSemiExtraBold,
        textAlign: 'auto',
    },
    TextExtraBold22: {
        fontSize: wp('9%'),
        fontFamily: fontNameSemiExtraBold,
        textAlign: 'auto',
    },
    TextBlack: {
        fontSize: wp('4%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextExtraLight6: {
        fontSize: wp('1%'),
        fontFamily: fontNameSmal,
        textAlign: 'auto',
    },
    TextExtraLight8: {
        fontSize: wp('2%'),
        fontFamily: fontNameSmal,
        textAlign: 'auto',
    },
    TextExtraLight10: {
        fontSize: wp('3%'),
        fontFamily: fontNameSmal,
        textAlign: 'auto',
    },
    TextExtraLight12: {
        fontSize: wp('4%'),
        fontFamily: fontNameSmal,
        textAlign: 'auto',
    },
    TextExtraLight13: {
        fontSize: wp('4.5%'),
        fontFamily: fontNameSmal,
        textAlign: 'auto',
    },
    TextExtraLight14: {
        fontSize: wp('5%'),
        fontFamily: fontNameSmal,
        textAlign: 'auto',
    },
    TextExtraLight16: {
        fontSize: wp('6%'),
        fontFamily: fontNameSmal,
        textAlign: 'auto',
    },
    TextExtraLight18: {
        fontSize: wp('7%'),
        fontFamily: fontNameSmal,
        textAlign: 'auto',
    },
    TextExtraLight20: {
        fontSize: wp('8%'),
        fontFamily: fontNameSmal,
        textAlign: 'auto',
    },
    TextLight6: {
        fontSize: wp('1%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextLight8: {
        fontSize: wp('2%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextLight10: {
        fontSize: wp('3%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextLight11: {
        fontSize: wp('3.5%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextLight12: {
        fontSize: wp('4%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextLight13: {
        fontSize: wp('4.5%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextLight14: {
        fontSize: wp('5%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextLight16: {
        fontSize: wp('6%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextLight18: {
        fontSize: wp('7%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextLight20: {
        fontSize: wp('8%'),
        fontFamily: fontNameMedium,
        textAlign: 'auto',
    },
    TextMedium: {
        fontSize: wp('4%'),
        fontFamily: fontNameSemiBold,
        textAlign: 'auto',
    },
    TextRegular9: {
        fontSize: wp('2.5%'),
        fontFamily: fontNameRegular,
        textAlign: 'auto',
    },
    TextRegular10: {
        fontSize: wp('3%'),
        fontFamily: fontNameRegular,
        textAlign: 'auto',
    },
    TextRegular11: {
        fontSize: wp('3.5%'),
        fontFamily: fontNameRegular,
        textAlign: 'auto',
    },
    TextRegular12: {
        fontSize: wp('4%'),
        fontFamily: fontNameRegular,
        textAlign: 'auto',
    },
    TextRegular13: {
        fontSize: wp('4.5%'),
        fontFamily: fontNameRegular,
        textAlign: 'auto',
    },
    TextRegular14: {
        fontSize: wp('5%'),
        fontFamily: fontNameRegular,
        textAlign: 'auto',
    },
    TextRegular15: {
        fontSize: wp('5.5%'),
        fontFamily: fontNameRegular,
        textAlign: 'auto',
    },
    TextRegular16: {
        fontSize: wp('6%'),
        fontFamily: fontNameRegular,
        textAlign: 'auto',
    },
    TextRegular18: {
        fontSize: wp('7%'),
        fontFamily: fontNameRegular,
        textAlign: 'auto',
    },


};
