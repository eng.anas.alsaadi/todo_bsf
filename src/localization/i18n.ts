import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {getLocales} from 'react-native-localize';
import en from './en';
import de from './de';

i18n.use(initReactI18next).init({
    lng: getLocales()[0].languageCode,
    fallbackLng: 'en',
    resources: {
        en: {
            translation: en,
        },
        de: {
            translation: de,
        },
    },
});
export default i18n;
