import * as React from 'react';
import {} from './types';
import {View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/main/HomeScreen';
import AddTaskScreen from '../screens/main/AddTaskScreen';
import SettingsScreen from '../screens/main/SettingsScreen';
import ManageTaskScreen from '../screens/main/ManageTaskScreen';
import {createStackNavigator} from '@react-navigation/stack';
import {NAVIGATION_HOME_SCREEN} from './types';
import {HOME_TAB_NAVIGATOR} from './types';
import BottomBar from './../components/templates/BottomBar';
import {ADD_TASK_TAB_NAVIGATOR} from './types';
import {SETTINGS_TAB_NAVIGATOR} from './types';
import {NAVIGATION_ADD_TASK_SCREEN} from './types';
import {NAVIGATION_SETTINGS_SCREEN} from './types';
import {NAVIGATION_MANAGE_TASK_SCREEN} from './types';
import {FC} from "react";
import {any} from "prop-types";


const Tab = createBottomTabNavigator();

const options = {headerMode: 'none', headerShown: false, tabBarVisible: false};
const HomeStack = createStackNavigator();
const AddTaskStack = createStackNavigator();
const SettingStack = createStackNavigator();
const HomeStackScreen: FC<any> = () => {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen options={options} name={NAVIGATION_HOME_SCREEN} component={HomeScreen}/>
            <HomeStack.Screen options={options} name={NAVIGATION_MANAGE_TASK_SCREEN} component={ManageTaskScreen}/>
        </HomeStack.Navigator>
    );
}
const AddTaskStackScreen: FC<any> = () => {
    return (
        <AddTaskStack.Navigator>
            <AddTaskStack.Screen options={options} name={NAVIGATION_ADD_TASK_SCREEN} component={AddTaskScreen}/>
        </AddTaskStack.Navigator>
    );
}
const SettingsStackScreen: FC<any> = () => {
    return (
        <SettingStack.Navigator>
            <SettingStack.Screen options={options} name={NAVIGATION_SETTINGS_SCREEN} component={SettingsScreen}/>
        </SettingStack.Navigator>
    );
}

const MainNavigator: FC<any> = () => {
    return (
        <View style={{flex: 1, backgroundColor: 'transparent'}}>
            <Tab.Navigator
                screenOptions={{
                    headerShown: false,
                }}
                tabBar={props => <View><BottomBar {...props} /></View>}

                backBehavior="history"
            >
                <Tab.Screen name={HOME_TAB_NAVIGATOR} component={HomeStackScreen}/>
                <Tab.Screen name={ADD_TASK_TAB_NAVIGATOR} component={AddTaskStackScreen}/>
                <Tab.Screen name={SETTINGS_TAB_NAVIGATOR} component={SettingsStackScreen}/>

            </Tab.Navigator>
        </View>

    );
};
export default MainNavigator;
