import React, {FC} from 'react';
import {createStackNavigator, CardStyleInterpolators} from '@react-navigation/stack';
import SplashScreen from '../screens/splash/SplashScreen';
import MainNavigator from "./MainNavigator";
import {NAVIGATION_SPLASH_SCREEN} from './types';
import {MAIN_NAVIGATOR} from './types';

const AppStack = createStackNavigator();

const AppNavigator: FC<any> = () => {

    return (
        <AppStack.Navigator
            screenOptions={{
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}>

            <AppStack.Screen name={NAVIGATION_SPLASH_SCREEN} component={SplashScreen}
                             options={{gestureEnabled: false}}/>

            <AppStack.Screen name={MAIN_NAVIGATOR} component={MainNavigator}
                             options={{gestureEnabled: false}}/>

        </AppStack.Navigator>

    );
};

export default (AppNavigator);

