import React, {FC, useEffect, useState} from 'react';
import {ScrollView} from 'react-native';
import MTextInput from '../../../components/global/MTextInput/index';
import MButton from '../../../components/global/MButton/index';
import Layout from '../../../components/templates/Layout';
import {useTranslation} from 'react-i18next';
import {addTaskRequest, addTaskReset, getTaskListRefresh} from '../../../store/task/actions';
import {useDispatch, useSelector} from 'react-redux';
import {gql} from '@apollo/client';
import {SUCCESS} from '../../../store/actionsType';
import {showAlertMessage} from '../../../store/appListener/actions';
import CardContainer from '../../../components/templates/CardContainer';

const AddTaskScreen: FC<any> = () => {
    const {t} = useTranslation();
    const dispatch = useDispatch();

    const {isLoadingAddTask, statusAddTask, errorsAddTask} = useSelector((state) => state.task);
    const [title, setTitle] = useState<string>('');
    const [description, setDescription] = useState<string>('');
    const [date, setDate] = useState<Date>(new Date());


    const addTask = () => {

        dispatch(addTaskRequest({
            gql: gql`
                mutation createTask($title:String,$description:String,$date:String,$status:Int){
                    createTask(title:$title,description:$description,date:$date,status:$status){
                        title
                    }
                }
            `,
            variables: {
                title,
                description,
                date,
                status: 1,
            },
        }));
    };


    useEffect(() => {

        if (statusAddTask === SUCCESS) {
            dispatch(addTaskReset());
            dispatch(getTaskListRefresh());
            dispatch(showAlertMessage({
                status: 200,
                message: t('taskAdded'),
            }));
            setTitle('');
            setDescription('');
        }
    }, [statusAddTask]);

    return <Layout title={t('addTask')}>
        <CardContainer>
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>

                <MTextInput
                    error={errorsAddTask && errorsAddTask?.title}
                    name={t('title')}
                    value={title}
                    placeholder={t('addTitle')}
                    onChangeText={(text) => setTitle(text)}
                />
                <MTextInput
                    error={errorsAddTask && errorsAddTask?.description}
                    name={t('description')}
                    value={description}
                    placeholder={t('addDescription')}
                    onChangeText={(text) => setDescription(text)}
                    multiline={true}
                />

                <MTextInput
                    type={'date'}
                    error={errorsAddTask && errorsAddTask?.date}
                    name={t('date')}
                    value={date}
                    placeholder={t('chooseDate')}
                    onChangeText={(text) => setDate(text)}
                />

            </ScrollView>
            <MButton loading={isLoadingAddTask} text={t('addTask')} onPress={addTask}/>
        </CardContainer>
    </Layout>;
};

export default AddTaskScreen;
