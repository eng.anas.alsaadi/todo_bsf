import React, {FC} from 'react';
import {View} from 'react-native';
import MText from '../../../components/global/MText/index';
import Layout from '../../../components/templates/Layout';
import TaskStatusList from '../../../components/list/TaskStatusList';
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';
import TasksList from '../../../components/list/TasksList';
import AppStyles from '../../../constants/AppStyles';
import {setStatusSelected} from '../../../store/task/actions';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {StatusType} from "../../../constants/InterfacesData";


const HomeScreen: FC<any> = () => {
    const dispatch = useDispatch();
    const {t} = useTranslation();
    const {statusTasks} = useSelector((state) => state.task);

    return <Layout title={t('dashboard')}>

        <View style={[AppStyles.CenterContent]}>
            <TaskStatusList selectedItem={statusTasks}
                            onPressItem={(item:StatusType) => dispatch(setStatusSelected(item))}/>
        </View>

        <MText type={'B_13'} text={statusTasks ? statusTasks.name + ' Tasks' : t('upcomingTask')} left
               moreStyle={{marginStart: wp(4), marginBottom: wp(4)}}/>

        <TasksList/>
    </Layout>;
};

export default HomeScreen;
