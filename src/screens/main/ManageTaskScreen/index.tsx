import React, {FC, useEffect, useState} from 'react';
import {View, ScrollView, TouchableOpacity} from 'react-native';
import MTextInput from '../../../components/global/MTextInput/index';
import MButton from '../../../components/global/MButton/index';
import Layout from '../../../components/templates/Layout';
import {useTranslation} from 'react-i18next';
import AppStyles from '../../../constants/AppStyles';
import {
    getTaskDetails,
    getTaskListRefresh, removeTaskRequest, removeTaskReset,
    updateTaskRequest, updateTaskReset,
} from '../../../store/task/actions';
import {useDispatch, useSelector} from 'react-redux';
import {gql} from '@apollo/client';
import {SUCCESS} from '../../../store/actionsType';
import {showAlertMessage} from '../../../store/appListener/actions';
import TaskStatusList from '../../../components/list/TaskStatusList';
import MText from '../../../components/global/MText';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import NavigationService from '../../../navigation/NavigationService';
import colors from '../../../constants/colors';
import Loader from '../../../components/global/Loaders/Loader';
import {StatusType} from "../../../constants/InterfacesData";

type Props = {
    route: any
}
const ManageTaskScreen: FC<Props> = ({route}) => {
    const {t} = useTranslation();
    const dispatch = useDispatch();

    const {taskDetails} = useSelector((state) => state.task);

    const {errorsUpdateTask, statusUpdateTask, isLoadingUpdateTask} = useSelector((state) => state.task);

    const {statusRemoveTask, isLoadingRemoveTask} = useSelector((state) => state.task);
    const [title, setTitle] = useState<string>('');
    const [description, setDescription] = useState<string>('');
    const [status, setStatus] = useState<StatusType>(null);
    const [date, setDate] = useState<any>(null);

    useEffect(() => {
        setTitle('');
        setDescription('');
        setStatus(null);
        dispatch(getTaskDetails({
            gql: gql`
                query task($id:Int){
                    task(id:$id){
                        title
                        description
                        id
                        status
                        date
                    }
                }
            `,
            variables: {
                id: parseInt(route.params.taskID),
            },
        }));
    }, []);

    useEffect(() => {

        if (taskDetails) {

            setTitle(taskDetails.title);
            setDescription(taskDetails.description);
            setStatus({id: taskDetails.status});
            setDate(taskDetails.date);
        }
    }, [taskDetails]);

    useEffect(() => {

        if (statusUpdateTask === SUCCESS) {

            dispatch(updateTaskReset());
            dispatch(getTaskListRefresh());
            dispatch(showAlertMessage({
                status: 200,
                message: t('taskUpdated'),
            }));
            NavigationService.goBack();
        }
    }, [statusUpdateTask]);

    useEffect(() => {

        if (statusRemoveTask === SUCCESS) {

            dispatch(removeTaskReset());
            dispatch(getTaskListRefresh());
            dispatch(showAlertMessage({
                status: 200,
                message: t('taskRemoved'),
            }));
            NavigationService.goBack();
        }
    }, [statusRemoveTask]);

    const saveChanges = () => {

        dispatch(updateTaskRequest({
            gql: gql`
                mutation updateTask($title:String,$description:String,$id:ID,$status:Int,$date:String){
                    updateTask(title:$title,description:$description,id:$id,status:$status,date:$date){
                        title
                    }
                }
            `,
            variables: {
                id: parseInt(route.params.taskID),
                title,
                description,
                status: parseInt(status.id),
                date,
            },
        }));

    };
    const removeTask = () => {

        dispatch(removeTaskRequest({
            gql: gql`
                mutation removeTask($id:ID){
                    removeTask(id:$id)
                }
            `,
            variables: {
                id: parseInt(route.params.taskID),
            },
        }));

    };

    return <Layout back title={t('manageTask')}>
        <View style={[AppStyles.BodyContainer, {flex: 1}]}>

            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>

                <MTextInput
                    error={errorsUpdateTask && errorsUpdateTask?.title}
                    name={t('title')}
                    value={title}
                    placeholder={t('addTitle')}
                    onChangeText={(text) => setTitle(text)}
                />
                <MTextInput
                    error={errorsUpdateTask && errorsUpdateTask?.description}
                    name={t('description')}
                    value={description}
                    placeholder={t('addDescription')}
                    onChangeText={(text) => setDescription(text)}
                    multiline={true}
                />
                <MTextInput
                    type={'date'}
                    error={errorsUpdateTask && errorsUpdateTask?.date}
                    name={t('date')}
                    value={date}
                    placeholder={t('chooseDate')}
                    onChangeText={(text) => setDate(text)}
                />

                <MText type={'B_12'} text={t('changeStatus')} moreStyle={{marginTop: wp(4)}}/>
                <TaskStatusList selectedItem={status} onPressItem={(item:StatusType) => setStatus(item)}/>


                {isLoadingRemoveTask ? <Loader/> :
                    <TouchableOpacity onPress={removeTask} style={{marginBottom: wp(4)}}><MText type={'B_13'}
                                                                                                text={t('removeTask')}
                                                                                                color={colors.Red}/></TouchableOpacity>}

            </ScrollView>
            <MButton loading={isLoadingUpdateTask} text={t('saveChanges')} onPress={saveChanges}/>
        </View>
    </Layout>;
};

export default ManageTaskScreen;
