import React, {FC, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import MText from '../../../components/global/MText/index';
import Layout from '../../../components/templates/Layout';
import OptionSetting from './../../../components/app/OptionSetting';
import {useTranslation} from 'react-i18next';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppStyles from '../../../constants/AppStyles';
import CardContainer from '../../../components/templates/CardContainer';

const SettingsScreen: FC<any> = () => {

    const {t, i18n} = useTranslation();
    const [selectedLanguage, setSelectedLanguage] =  useState<string>(i18n.language);

    const changeLanguage = (lang:string) => {
        setSelectedLanguage(lang);
        i18n.changeLanguage(lang);
        AsyncStorage.setItem('lang', lang);
    };

    return <Layout title={t('settings')}>

        <CardContainer>
            <MText type={'B_13'} text={t('changeLanguage')} left/>

            <OptionSetting name={t('language.en')} selected={selectedLanguage === 'en'}
                           onPressItem={() => changeLanguage('en')}/>
            <OptionSetting name={t('language.de')} selected={selectedLanguage === 'de'}
                           onPressItem={() => changeLanguage('de')}/>

        </CardContainer>

    </Layout>;
};

export default SettingsScreen;
