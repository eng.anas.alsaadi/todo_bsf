import React, {FC, useEffect} from 'react';
import Splash from 'react-native-splash-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {MAIN_NAVIGATOR} from '../../../navigation/types';
import {useTranslation} from 'react-i18next';

const SplashScreen: FC<any> = ({navigation}) => {
    const {i18n} = useTranslation();


    useEffect(() => {
        AsyncStorage.getItem('lang')
            .then(lang => {
                i18n.changeLanguage(lang);
            })
            .catch(err => {

                i18n.changeLanguage('en');
            });

        Splash.hide();
        navigation.navigate(MAIN_NAVIGATOR);
    }, []);
    return null;
};

export default SplashScreen;
