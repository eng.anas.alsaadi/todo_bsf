export const REQUEST = 'REQUEST';
export const LOADING = 'LOADING';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';
export const REFRESH = 'REFRESH';


export const GET_TASK_DETAILS_RESET = 'getTaskDetailsReset';
export const GET_TASK_DETAILS_REQUEST = 'getTaskDetailsRequest';
export const GET_TASK_DETAILS_LOADING = 'getTaskDetailsLoading';
export const GET_TASK_DETAILS_SUCCESS = 'getTaskDetailsSuccess';
export const GET_TASK_DETAILS_FAIL = 'getTaskDetailsFail';

export const ADD_TASK_RESET = 'addTaskReset';
export const ADD_TASK_REQUEST = 'addTaskRequest';
export const ADD_TASK_LOADING = 'addTaskLoading';
export const ADD_TASK_SUCCESS = 'addTaskSuccess';
export const ADD_TASK_FAIL = 'addTaskFail';

export const UPDATE_TASK_RESET = 'updateTaskReset';
export const UPDATE_TASK_REQUEST = 'updateTaskRequest';
export const UPDATE_TASK_LOADING = 'updateTaskLoading';
export const UPDATE_TASK_SUCCESS = 'updateTaskSuccess';
export const UPDATE_TASK_FAIL = 'updateTaskFail';

export const REMOVE_TASK_RESET = 'removeTaskReset';
export const REMOVE_TASK_REQUEST = 'removeTaskRequest';
export const REMOVE_TASK_LOADING = 'removeTaskLoading';
export const REMOVE_TASK_SUCCESS = 'removeTaskSuccess';
export const REMOVE_TASK_FAIL = 'removeTaskFail';

export const GET_TASKS_REFRESH = 'getTasksRefresh';
export const GET_TASKS_RESET = 'getTasksReset';
export const GET_TASKS_REQUEST = 'getTasksRequest';
export const GET_TASKS_LOADING = 'getTasksLoading';
export const GET_TASKS_SUCCESS = 'getTasksSuccess';
export const GET_TASKS_FAIL = 'getTasksFail';


export const SHOW_ALERT_MESSAGE = 'showAlertMessage';
export const HIDE_ALERT_MESSAGE = 'hideAlertMessage';
export const SET_SELECTED_STATUS = 'setStatusSelected';


