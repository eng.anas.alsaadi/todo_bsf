import {ApolloClient, InMemoryCache} from '@apollo/client';
import {RequestType} from './../../constants/InterfacesData';


const parseErrorRequest = (error:any) => {

    if (error.message === 'validation') {
        return ({
            status: 403,
            data: null,
            errors: error.graphQLErrors[0].extensions.validation,
        });
    }
};

export const requestAction = async (request:RequestType) => {

    const apolloClient = new ApolloClient({
        uri: 'http://localhost:8000/graphql',
        cache: new InMemoryCache(),
    });

    return new Promise(
        (resolve, reject) => {


            if (request.type === 'mutate') {
                apolloClient.mutate({
                    mutation: request.payload.gql,
                    variables: request.payload.variables,
                }).then(({error, data}) => {


                    resolve({
                        status: 200,
                        data: data,
                    });

                }).catch(error => {

                    resolve(parseErrorRequest(error));

                });
            } else {
                apolloClient.query({
                    query: request.payload.gql,
                    variables: request.payload.variables,
                }).then(({error, data}) => {

                    resolve({
                        status: 200,
                        data: data,
                    });

                }).catch(error => {

                    resolve(parseErrorRequest(error));
                });

            }


        },
    );


};
