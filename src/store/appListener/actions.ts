import {SHOW_ALERT_MESSAGE} from '../actionsType';
import {HIDE_ALERT_MESSAGE} from '../actionsType';


export const showAlertMessage = (payload: any) => ({
    type: SHOW_ALERT_MESSAGE,
    payload,
});
export const hideAlertMessage = (payload: any) => ({
    type: HIDE_ALERT_MESSAGE,
    payload,
});
