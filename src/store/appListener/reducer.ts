import {
    HIDE_ALERT_MESSAGE,
    SHOW_ALERT_MESSAGE,
} from '../actionsType';
import {ReduxType} from "../../constants/InterfacesData";

const INITIAL_STATE = {

        visible_alert: false,
        status_alert: false,
        message_alert: '',


    }
;

export default (state = INITIAL_STATE, {type, payload}: ReduxType) => {
    switch (type) {


        case SHOW_ALERT_MESSAGE:
            return {
                ...state,
                visible_alert: true,
                status_alert: payload.status,
                message_alert: payload?.message,
            };
        case HIDE_ALERT_MESSAGE:
            return {
                ...state,
                visible_alert: false,
                status_alert: null,
                message_alert: '',
            };


        default:
            return state;
    }
};


