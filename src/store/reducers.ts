import {combineReducers} from 'redux';

import appListenerReducer from './appListener/reducer';
import taskReducer from './task/reducer';

const appReducer = combineReducers({
    appListener: appListenerReducer,
    task:taskReducer,

});

export default appReducer;
