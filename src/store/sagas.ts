import {fork} from 'redux-saga/effects';
import appListenerSagas from './appListener/sagas';
import taskSagas from './task/sagas';

export default function* root() {
    yield fork(appListenerSagas);
    yield fork(taskSagas);
}
