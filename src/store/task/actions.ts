import {ADD_TASK_REQUEST} from '../actionsType';
import {ADD_TASK_RESET} from '../actionsType';
import {GET_TASKS_REQUEST} from '../actionsType';
import {GET_TASKS_RESET} from '../actionsType';
import {GET_TASKS_REFRESH} from '../actionsType';
import {GET_TASK_DETAILS_REQUEST} from '../actionsType';
import {SET_SELECTED_STATUS} from '../actionsType';
import {UPDATE_TASK_REQUEST} from '../actionsType';
import {UPDATE_TASK_RESET} from '../actionsType';
import {REMOVE_TASK_REQUEST} from '../actionsType';
import {REMOVE_TASK_RESET} from '../actionsType';


/// One Work
export const addTaskRequest = (payload: any) => ({
    type: ADD_TASK_REQUEST,
    payload,
});
export const addTaskReset = (payload: any) => ({
    type: ADD_TASK_RESET,
    payload,
});

export const updateTaskRequest = (payload: any) => ({
    type: UPDATE_TASK_REQUEST,
    payload,
});
export const updateTaskReset = (payload: any) => ({
    type: UPDATE_TASK_RESET,
    payload,
});

export const removeTaskRequest = (payload: any) => ({
    type: REMOVE_TASK_REQUEST,
    payload,
});
export const removeTaskReset = (payload: any) => ({
    type: REMOVE_TASK_RESET,
    payload,
});

export const getTaskDetails = (payload: any) => ({
    type: GET_TASK_DETAILS_REQUEST,
    payload,
});


//// List Work
export const getTaskListRequest = (payload: any) => ({
    type: GET_TASKS_REQUEST,
    payload,
});

export const getTaskListReset = (payload: any) => ({
    type: GET_TASKS_RESET,
    payload,
});
export const getTaskListRefresh = (payload: any) => ({
    type: GET_TASKS_REFRESH,
    payload,
});


//// Status Task For List Of Tasks

export const setStatusSelected = (payload: any) => ({
    type: SET_SELECTED_STATUS,
    payload,
});
