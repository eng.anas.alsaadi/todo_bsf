import {
    ADD_TASK_FAIL,
    ADD_TASK_LOADING,
    ADD_TASK_RESET,
    ADD_TASK_SUCCESS,
    FAILURE, GET_TASK_DETAILS_FAIL, GET_TASK_DETAILS_LOADING, GET_TASK_DETAILS_RESET,
    GET_TASK_DETAILS_SUCCESS,
    GET_TASKS_FAIL,
    GET_TASKS_LOADING,
    GET_TASKS_REFRESH,
    GET_TASKS_RESET,
    GET_TASKS_SUCCESS,
    LOADING,
    REFRESH, REMOVE_TASK_FAIL, REMOVE_TASK_LOADING, REMOVE_TASK_RESET, REMOVE_TASK_SUCCESS, SET_SELECTED_STATUS,
    SUCCESS, UPDATE_TASK_FAIL, UPDATE_TASK_LOADING, UPDATE_TASK_REQUEST, UPDATE_TASK_RESET, UPDATE_TASK_SUCCESS,
} from '../actionsType';
import {ReduxType} from '../../constants/InterfacesData';

const INITIAL_STATE = {

    isLoadingAddTask: false,
    statusAddTask: '',
    errorsAddTask: [],

    isLoadingUpdateTask: false,
    statusUpdateTask: '',
    errorsUpdateTask: [],

    isLoadingRemoveTask: false,
    statusRemoveTask: '',
    errorsRemoveTask: [],

    isLoadingGetTaskDetails: false,
    statusGetTaskDetails: '',
    errorsGetTaskDetails: [],
    taskDetails: null,


    isLoadingGetTasks: false,
    statusGetTasks: '',
    errorsGetTasks: [],
    tasks: [],
    nextPageGetTasks: 1,
    haveNextPageGetTasks: true,

    statusTasks: null,

};

export default (state = INITIAL_STATE, {type, payload}:ReduxType) => {
    switch (type) {



        ////////

        case ADD_TASK_LOADING:
            return {
                ...state,
                isLoadingAddTask: true,
                statusAddTask: LOADING,
                errorsAddTask: [],
            };

        case ADD_TASK_SUCCESS:

            return {
                ...state,
                isLoadingAddTask: false,
                statusAddTask: SUCCESS,
                errorsAddTask: [],

            };


        case ADD_TASK_FAIL:

            return {
                ...state,
                isLoadingAddTask: false,
                statusAddTask: FAILURE,
                errorsAddTask: payload.errors,
            };

        case ADD_TASK_RESET:

            return {
                ...state,
                isLoadingAddTask: false,
                statusAddTask: '',
                errorsAddTask: [],
            };


/////
        case UPDATE_TASK_LOADING:
            return {
                ...state,
                isLoadingUpdateTask: true,
                statusUpdateTask: LOADING,
                errorsUpdateTask: [],
            };

        case UPDATE_TASK_SUCCESS:

            return {
                ...state,
                isLoadingUpdateTask: false,
                statusUpdateTask: SUCCESS,
                errorsUpdateTask: [],

            };


        case UPDATE_TASK_FAIL:

            return {
                ...state,
                isLoadingUpdateTask: false,
                statusUpdateTask: FAILURE,
                errorsUpdateTask: payload.errors,
            };

        case UPDATE_TASK_RESET:

            return {
                ...state,
                isLoadingUpdateTask: false,
                statusUpdateTask: '',
                errorsUpdateTask: [],
            };


/////
        case REMOVE_TASK_LOADING:
            return {
                ...state,
                isLoadingRemoveTask: true,
                statusRemoveTask: LOADING,
                errorsRemoveTask: [],
            };

        case REMOVE_TASK_SUCCESS:

            return {
                ...state,
                isLoadingRemoveTask: false,
                statusRemoveTask: SUCCESS,
                errorsRemoveTask: [],

            };


        case REMOVE_TASK_FAIL:

            return {
                ...state,
                isLoadingRemoveTask: false,
                statusRemoveTask: FAILURE,
                errorsRemoveTask: payload.errors,
            };

        case REMOVE_TASK_RESET:

            return {
                ...state,
                isLoadingRemoveTask: false,
                statusRemoveTask: '',
                errorsRemoveTask: [],
            };


/////
        case GET_TASK_DETAILS_LOADING:
            return {
                ...state,
                isLoadingGetTaskDetails: true,
                statusGetTaskDetails: LOADING,
                errorsGetTaskDetails: [],
            };

        case GET_TASK_DETAILS_SUCCESS:

            return {
                ...state,
                isLoadingGetTaskDetails: false,
                statusGetTaskDetails: SUCCESS,
                errorsGetTaskDetails: [],
                taskDetails: payload.data,

            };


        case GET_TASK_DETAILS_FAIL:

            return {
                ...state,
                isLoadingGetTaskDetails: false,
                statusGetTaskDetails: FAILURE,
                errorsGetTaskDetails: payload.errors,
            };

        case GET_TASK_DETAILS_RESET:

            return {
                ...state,
                isLoadingGetTaskDetails: false,
                statusGetTaskDetails: '',
                errorsGetTaskDetails: [],
            };


        ////////

        case GET_TASKS_LOADING:
            return {
                ...state,
                isLoadingGetTasks: true,
                statusGetTasks: LOADING,
                errorsGetTasks: [],
            };

        case GET_TASKS_SUCCESS:

            return {
                ...state,
                isLoadingGetTasks: false,
                statusGetTasks: SUCCESS,
                errorsGetTasks: [],
                tasks: [...state.tasks, ...payload.data.data],
                nextPageGetTasks: payload.data.data.length > 0 ? state.nextPageGetTasks + 1 : state.nextPageGetTasks,
                haveNextPageGetTasks: payload.data.data.length > 0,

            };


        case GET_TASKS_FAIL:

            return {
                ...state,
                isLoadingGetTasks: false,
                statusGetTasks: FAILURE,
                errorsGetTasks: payload.errors,
                nextPageProductsList: 1,
                haveNextPageProductsList: true,
            };

        case GET_TASKS_RESET:

            return {
                ...state,
                isLoadingGetTasks: false,
                statusGetTasks: '',
                errorsGetTasks: [],
                tasks: [],
            };

        case GET_TASKS_REFRESH:

            return {
                ...state,
                statusGetTasks: REFRESH,
            };


        ////////
        case SET_SELECTED_STATUS:

            return {
                ...state,
                statusTasks: payload,
            };


        ////////

        default:
            return state;
    }
};


