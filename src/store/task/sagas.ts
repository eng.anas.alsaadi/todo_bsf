import {takeLatest, call, put} from 'redux-saga/effects';
import {
    ADD_TASK_FAIL,
    ADD_TASK_LOADING,
    ADD_TASK_REQUEST,
    ADD_TASK_SUCCESS,
    GET_TASK_DETAILS_FAIL,
    GET_TASK_DETAILS_LOADING,
    GET_TASK_DETAILS_REQUEST,
    GET_TASK_DETAILS_SUCCESS,
    GET_TASKS_FAIL,
    GET_TASKS_LOADING,
    GET_TASKS_REQUEST,
    GET_TASKS_SUCCESS, REMOVE_TASK_FAIL, REMOVE_TASK_LOADING,
    REMOVE_TASK_REQUEST, REMOVE_TASK_SUCCESS,
    UPDATE_TASK_FAIL,
    UPDATE_TASK_LOADING,
    UPDATE_TASK_REQUEST,
    UPDATE_TASK_SUCCESS,
} from '../actionsType';
import {requestAction} from '../api/api';
import {RequestType} from "../../constants/InterfacesData";


function* addTaskAction({payload}: any) {
    try {

        yield put({type: ADD_TASK_LOADING});

        var requestConfig = {
            type: 'mutate',
            payload: payload,
        };
        const response = yield call(requestAction, requestConfig);

        if (response.status === 200) {

            yield put({
                type: ADD_TASK_SUCCESS, payload: {
                    data: response.data,
                },
            });
        } else {
            yield put({
                type: ADD_TASK_FAIL, payload: {
                    errors: response.errors,
                },
            });

        }


    } catch (error) {

        yield put({
            type: ADD_TASK_FAIL, payload: {
                data: null,
            },
        });
    }
}

function* getTaskDetailsAction({payload}: any) {
    try {

        yield put({type: GET_TASK_DETAILS_LOADING});

        var requestConfig = {
            type: 'query',
            payload: payload,
        };
        const response = yield call(requestAction, requestConfig);

        if (response.status === 200) {

            yield put({
                type: GET_TASK_DETAILS_SUCCESS, payload: {
                    data: response.data.task,
                },
            });
        } else {
            yield put({
                type: GET_TASK_DETAILS_FAIL, payload: {
                    errors: response.errors,
                },
            });

        }


    } catch (error) {

        yield put({
            type: GET_TASK_DETAILS_FAIL, payload: {
                data: null,
            },
        });
    }
}

function* updateTaskAction({payload}: any) {
    try {

        yield put({type: UPDATE_TASK_LOADING});

        var requestConfig = {
            type: 'mutate',
            payload: payload,
        };
        const response = yield call(requestAction, requestConfig);

        if (response.status === 200) {

            yield put({
                type: UPDATE_TASK_SUCCESS, payload: {
                    data: response.data,
                },
            });
        } else {
            yield put({
                type: UPDATE_TASK_FAIL, payload: {
                    errors: response.errors,
                },
            });

        }


    } catch (error) {

        yield put({
            type: UPDATE_TASK_FAIL, payload: {
                data: null,
            },
        });
    }
}

function* removeTaskAction({payload}: any) {
    try {

        yield put({type: REMOVE_TASK_LOADING});

        var requestConfig = {
            type: 'mutate',
            payload: payload,
        };
        const response = yield call(requestAction, requestConfig);

        if (response.status === 200) {

            yield put({
                type: REMOVE_TASK_SUCCESS, payload: {
                    data: response.data,
                },
            });
        } else {
            yield put({
                type: REMOVE_TASK_FAIL, payload: {
                    errors: response.errors,
                },
            });

        }


    } catch (error) {

        yield put({
            type: REMOVE_TASK_FAIL, payload: {
                data: null,
            },
        });
    }
}

function* getTasksAction({payload}: any) {
    try {

        yield put({type: GET_TASKS_LOADING});

        var requestConfig = {
            type: 'query',
            payload: payload,
        };
        const response = yield call(requestAction, requestConfig);

        if (response.status === 200) {

            yield put({
                type: GET_TASKS_SUCCESS, payload: {
                    data: response.data.tasks,
                },
            });
        } else {
            yield put({
                type: GET_TASKS_FAIL, payload: {
                    errors: response.errors,
                },
            });

        }


    } catch (error) {

        yield put({
            type: GET_TASKS_FAIL, payload: {
                data: null,
            },
        });
    }
}

export default function* watcherSaga() {
    yield takeLatest(ADD_TASK_REQUEST, addTaskAction);
    yield takeLatest(GET_TASKS_REQUEST, getTasksAction);
    yield takeLatest(GET_TASK_DETAILS_REQUEST, getTaskDetailsAction);
    yield takeLatest(UPDATE_TASK_REQUEST, updateTaskAction);
    yield takeLatest(REMOVE_TASK_REQUEST, removeTaskAction);

}
